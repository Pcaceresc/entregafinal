import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { DocenteComponent } from './docente/docente.component';

const rutas: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'docente', component: DocenteComponent },
  { path: '', component: InicioComponent, pathMatch: 'full'},
  { path: '**',redirectTo: 'inicio' , pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(rutas)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
