import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Docente } from '../models/docente';

@Injectable({
  providedIn: 'root'
})
export class ConexionService {

  selectedDocente: Docente;
  docentes: Docente[];
  readonly URL_API = 'http://localhost:3000/api/docentes';

  constructor(private http: HttpClient){ 
    this.selectedDocente = new Docente();
  }

  getDocentes() {
    return this.http.get(this.URL_API);
  }

  postDocente(Docente: Docente) {
    return this.http.post(this.URL_API, Docente);
  }

  putDocente(docente: Docente) {
    return this.http.put(this.URL_API + `/${docente._id}`, docente);
  }

  deleteDocente(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
