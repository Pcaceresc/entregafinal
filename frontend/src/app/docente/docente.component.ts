import { Component, OnInit } from '@angular/core';
import { ConexionService } from '../services/conexion.service';
import { NgForm } from '@angular/forms';
import { Docente } from '../models/docente';
import { ToastrService } from 'ngx-toastr';

declare var M: any;

@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.scss']
})
export class DocenteComponent implements OnInit {

   
  constructor(public conexion: ConexionService,
    private toastr: ToastrService){
    
  }

  docenteLista : Docente[];

  ngOnInit() {
    this.getDocentes();
  }

  addDocente(form: NgForm) {
    if (form.value._id) {
      this.conexion.putDocente(form.value)
        .subscribe(res => {
          this.resetForm(form);
          this.getDocentes();
        })
    } else {
      this.conexion.postDocente(form.value)
      .subscribe(res => {
        this.resetForm(form);
        this.getDocentes();
      })
    }
  }

  getDocentes() {
    this.conexion.getDocentes()
      .subscribe(res => {
        this.conexion.docentes = res as Docente[];
        console.log(res)
      })
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.conexion.selectedDocente = new Docente();
    }
  }

  editDocente(docente: Docente) {
    this.conexion.selectedDocente = Object.assign({}, docente);
  }

  deleteDocente(_id : string) {
    if(confirm('Estas seguro que quieres eliminarlo?')) {
      this.conexion.deleteDocente(_id)
        .subscribe(res => {
          this.getDocentes();
        })
    }
  }
}
