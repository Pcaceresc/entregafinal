export class Docente {
    
    _id: string;
    nombre: string;
    descripcion: string;
    edad: number;
    ramos: string;
    ventajas: string;
    desventajas: string;

}
