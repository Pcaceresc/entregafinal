const Docente = require('../models/docentes');
const docentesCtrl = {};


docentesCtrl.getDocentes = async (req,res) => {
    const docentes = await Docente.find();
    res.json(docentes);
};

docentesCtrl.createDocente = async (req, res) => {
    const docente = new Docente(req.body);
    await docente.save();
    res.json({
        'status' : 'Docente Saved'
    }); 
};

docentesCtrl.getDocente = async (req, res) => {
    const docente = await Docente.findById(req.params.id);
    res.json(docente);
}

docentesCtrl.editDocente = async (req, res) => {
    const docente = {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        edad: req.body.edad,
        ramos: req.body.ramos,
        ventajas: req.body.ventajas,
        desventajas : req.body.desventajas
    };
    await Docente.findByIdAndUpdate(req.params.id, {$set: docente}, {new: true});
    res.json({status: 'Docente Update'});
}

docentesCtrl.deleteDocente = async (req, res) => {
    await Docente.findByIdAndRemove(req.params.id);
    res.json({status: 'Docente Deleted'});
}


module.exports = docentesCtrl;