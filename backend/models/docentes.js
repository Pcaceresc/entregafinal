const mongoose = require('mongoose');
const { Schema } = mongoose;


const DocentesSchema = new Schema({
    
    nombre: {type: String, requiere: true},
    descripcion: {type: String, requiere: true},
    edad: {type: Number, requiere: true},
    ramos: {type: String, requiere: true},
    ventajas: {type: String, requiere: true},
    desventajas: {type: String, requiere: true}
});

module.exports = mongoose.model('Docentes', DocentesSchema);