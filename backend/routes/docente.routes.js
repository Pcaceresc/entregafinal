const express = require('express');
const router = express.Router();

const docenteCtrl = require('../controllers/docentes.controller');

router.get('/' , docenteCtrl.getDocentes);
router.post('/' , docenteCtrl.createDocente);
router.get('/:id' , docenteCtrl.getDocente);
router.put('/:id' , docenteCtrl.editDocente);
router.delete('/:id' , docenteCtrl.deleteDocente);

module.exports = router;